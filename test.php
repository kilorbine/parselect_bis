<?php

if ($_POST['data'])
{
	$match = [];
	preg_match_all("/coordonnees\(\d*,\d*,\d*\)/", $_POST['data'], $match);
	//var_dump($match);
	$parcels = [];
	foreach ($match[0] as $key => $value)
	{
		$data = substr($value, 12, (strlen($value)-13));
		$data = explode(',', $data);
		$parcels[$data[0]] = [
			'amount' => $data[1],
			'access' => $data[2]
		];
	}

	$amount = [];
	$access = [];
	echo '
	Toutes les parcelles avec ressources > 0 : 
	<select>
	';
	foreach ($parcels as $id => $value)
	{
		if ($parcels[$id]['amount'] != 0) {
			echo '<option>Parcelle '.$id.' : valeur : '.$value['amount'].'; accessibilité : '.$value['access'].'</option>';
		}
		
		$amount[$value['amount']][] = $id;
		$access[$value['access']][] = $id;
	}
	echo '
	</select>';
	krsort($amount);
	krsort($access);

	echo '<br/>';

	echo 'Les parcelles triées avec les meilleurs ressources : <select>
	';
	foreach ($amount as $key => $value)
	{
		foreach ($value as $id)
		{
			if ($parcels[$id]['amount'] == 0) {
				continue;
			}
			echo '<option>Parcelle '.$id.' : valeur : '.$parcels[$id]['amount'].'; accessibilité : '.$parcels[$id]['access'].'</option>';
		}
	}
	echo '
	</select><br/>';

	echo 'Les parcelles triées avec la meilleure accessibilité : <select>
	';
	foreach ($access as $key => $value)
	{
		foreach ($value as $id)
		{
			if ($parcels[$id]['amount'] == 0) {
				continue;
			}
			echo '<option>Parcelle '.$id.' : valeur : '.$parcels[$id]['amount'].'; accessibilité : '.$parcels[$id]['access'].'</option>';
		}
	}
	echo '
	</select><br/>';

}



echo '

<form action="" method="post">
Code source de la page : 
<textarea name="data"></textarea><br/>
<input type="submit"/>
</form>
';

?>